from . import mock, MockRequest, BaseApiTestCase
from gateway_client.booking import BookingAPIClient as BookingService
import requests
import datetime
import json
from gateway_client.client3 import to_graphql


class BookingServiceTestCase(BaseApiTestCase):
    module_name = "None"

    def setUp(self):
        super(BookingServiceTestCase, self).setUp()
        self.patcher = mock.patch('gateway_client.requests.get')
        self.patcher2 = mock.patch('gateway_client.requests.post')

        self.maxDiff = None
        self.mock_get = self.patcher.start()
        self.mock_post = self.patcher2.start()
        self.booking_service_url = 'http://localhost:5000/'
        self.instance = BookingService(url=self.booking_service_url)

    def test_get_bookings(self):
        response = {'bookings': [{
            'last_session':
                '2016-10-12T04:03:00+00:00',
            'first_session':
                '2016-10-12T00:03:00+00:00',
            'order':
                '321D44WER44T',
            'booking_type':
                1,
            'student_no':
                1,
            'skill_display':
                'Yoruba, English and French',
            'status':
                1,
            'bank_price':
                3000.0
        }, {
            'last_session':
                '2016-10-15T04:03:00+00:00',
            'first_session':
                '2016-10-12T00:03:00+00:00',
            'order':
                'ABCDEFGHIJKL',
            'booking_type':
                1,
            'student_no':
                1,
            'skill_display':
                'Yoruba, English and French',
            'status':
                1,
            'bank_price':
                15500.0
        }]}

        self.mock_post.return_value = self.mock_response(response)
        result = self.instance.get_bookings("gofaniyi", key="tutor")
        self.mock_post.assert_called_once_with(
            "{0}graphql".format(self.booking_service_url),
            data=json.dumps({
                'query': to_graphql(self.instance.service.get_bookings_query(['tutor'], False)),
                'operationName': "getUserBookings",
                'variables': {'tutor': 'gofaniyi'},
            }),
            headers={'Accept': 'application/json', 'Content-Type': 'application/json'})

        self.assertEqual(result, response['bookings'])

    def test_get_booking(self):
        response = {
            "booking": {
                'last_session':
                '2016-10-12T04:03:00+00:00',
                'first_session':
                '2016-10-12T00:03:00+00:00',
                'order':
                '12E3ED334F54',
                'student_no':
                1,
                'skill_display':
                'Yoruba, English and French',
                'status':
                1,
                'total_price':
                3000.0
            }
        }
        self.mock_post.return_value = self.mock_response(response)
        result = self.instance.get_booking("12E3ED334F54", fields=[
                                           'order', 'first_session', 'last_session', 'skill_display', 'total_price'])

        self.mock_post.assert_called_once_with(
            "{0}graphql".format(self.booking_service_url),
            data=json.dumps({
                'query': to_graphql(self.instance.service.get_booking_query(with_sessions=False, fields=['order', 'first_session', 'last_session', 'skill_display', 'total_price'])),
                'operationName': "getBooking",
                'variables': {'order': '12E3ED334F54'},
            }),
            headers={'Accept': 'application/json', 'Content-Type': 'application/json'})

        self.assertEqual(result, response['booking'])


    def test_update_sessions_status(self):
        response = {'update_booking': {'exception': None, 'data': {'bookingsession_set': [{'status': 3}], 'reviewed': False, 'order': '12E3ED334F54', 'reviews_given': [{'review_type': 2, 'review': 'I liked the client.', 'score': 4}], 'status': 7, 'delivered_on': '2017-12-08T17:02:57.670540+00:00'}, 'message': None, 'errors': None}}
        
        sessions = [{
                "index": 1,
                "status": "completed"
        }]
        review = {
                "review": 'I liked the client.',
                'score': 4,
                'review_type': 2,
            }

        self.mock_post.return_value = self.mock_response(response)
        result, is_valid = self.instance.update_sessions_status(order="12E3ED334F54", sessions=sessions,review=review)

        kwargs = {
            'order': "12E3ED334F54",
            'sessions': sessions,
            'review': review
        }
        
        self.mock_post.assert_called_once_with(
            "{0}graphql".format(self.booking_service_url),
            data=json.dumps({
                'query': to_graphql(self.instance.service.update_booking_query()),
                'operationName': "updateBooking",
                'variables': kwargs,
            }),
            headers={'Accept': 'application/json', 'Content-Type': 'application/json'})
        # import pdb; pdb.set_trace()
        self.assertEqual(result, response['update_booking'])


    def test_close_booking(self):
        response = {'close_booking': 
                        {'message': None, 'data': {'status': 3}, 'errors': None}
                    }
        
        self.mock_post.return_value = self.mock_response(response)
        result, is_valid = self.instance.close_booking(order="12E3ED334F54")
        
        self.mock_post.assert_called_once_with(
            "{0}graphql".format(self.booking_service_url),
            data=json.dumps({
                'query': to_graphql(self.instance.service.close_booking_query()),
                'operationName': "closeBooking",
                'variables': {"order": "12E3ED334F54"},
            }),
            headers={'Accept': 'application/json', 'Content-Type': 'application/json'})
        
        self.assertEqual(result, response['close_booking'])
        
    def test_create_booking(self):
        response = {"create_booking":
                    {'message': 'Booking Created Successfully', 'data': {'order': 'Q6ES3D4IF7TO', 'status': 1}}}
        self.mock_post.return_value = self.mock_response(response)
        data = {
            'sessions': [{
                "date": "2017-10-12",
                "start": "8:00",
                "end": "11:00",
                "price": 4500,
                "no_of_hours": 3,
                "student_no": 1,
            }],
            'tutor':
                "jamesd",
            'subjects': ["English Language", "Yoruba"],
            'percentage_split':
                85,
            'client':
                "john@example.com",
        }

        result,is_valid = self.instance.create_booking(
            client=data['client'],
            tutor=data['tutor'],
            percentage_split=data['percentage_split'],
            subjects=data['subjects'],
            sessions=data['sessions'])

        # self.mock_post.assert_called_once_with(
        #     "{0}graphql".format(self.booking_service_url),
        #     data=json.dumps({
        #         'query': to_graphql(self.instance.service.create_booking_query()),
        #         'operationName': "createBooking",
        #         'variables': data,
        #     }),
        #     headers={'Accept': 'application/json', 'Content-Type': 'application/json'})
         
      
        self.assertTrue(is_valid)
        self.assertCountEqual(result, response['create_booking'])
