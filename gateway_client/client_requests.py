import os
import requests
from . import (
    BaseClient, TuteriaApiException, get_field_value, FormField,
    BaseGraphClient)
from .email import email_and_sms_helper
try:
    from cached_property import cached_property
except ImportError:
    from django.utils.functional import cached_property


class BaseRequestService(BaseClient):
    def __init__(self, url=None, broker_url=None):
        self.base_url = os.environ.get('REQUEST_SERVICE_URL')
        # self.broker_url = os.environ.get('MESSAGING_URL')
        if url:
            self.base_url = url
        # if broker_url:
        #     self.broker_url = broker_url
        if not self.base_url:
            raise TuteriaApiException("Url for Request Service was not found.")


class RequestService(BaseRequestService):
    path = 'requests/'

    def __init__(self, url=None, broker_url=None):
        super(RequestService, self).__init__(url, broker_url)
        self.deposit_service = DepositService(self.base_url)
        self.get_request_class()

    def add_path(self, path):
        self.path = path

    def resolve_path(self, **kwargs):
        sid = kwargs.pop('request_slug', '')
        if sid:
            sid = '{}/'.format(sid)
        return sid, kwargs

    def create_new_request(self, user_details):
        return self._fetch_request('POST', 'create_new_request/', json=user_details)

    def update_request_with_user(self, request_id, user_id):
        return self.generic_update_of_request(
            request_id, {'user': user_id})

    def get_per_hour_price(self, req_id, cost):
        response = self._fetch_request(
            'GET', '{}/get_per_hour_price/'.format(req_id), params={
                'cost': cost
            }
        )
        return response['ttp']

    def get_instance(self, **kwargs):
        response = self._fetch_request(
            'GET', 'request_from_slug/', params=kwargs)
        if response == {}:
            raise requests.exceptions.RequestException("No instance found")
        return response['instance'], response['selected_tutor']

    def get_completed_requests(self, **kwargs):
        new_params = kwargs
        new_params.update(status=2)
        return self._fetch_request('GET', 'requests_by_status/',
                                   params=new_params)

    def update_request_with_subject(self, request_id, subject):
        return self.generic_update_of_request(request_id,
                                              {'request_subjects': [subject]})

    def generic_update_of_request(self, request_id, params, **kwargs):
        response = self._fetch_request('PATCH', "{}/".format(request_id),
                                       json=params)
        if kwargs.get('update_pool'):
            self.add_tutor_to_client_request_pool(
                request_id, response['tutor_slug'], response['budget'],
                update_status=True)
        if kwargs.get('create_booking'):
            return self.create_new_booking(request_id)
        return response

    @cached_property
    def get_levels_and_rates(self):
        return self._fetch_request('GET', 'levels_and_price_rates/')

    def get_levels_of_students(self):
        return self.get_levels_and_rates['levels']

    def get_rates(self):
        return self.get_levels_and_rates['rates']

    def get_pricings(self, id, plan):
        return self._fetch_request(
            'GET', 'get_pricings/',
            params=dict(request_id=id, plan=plan, featured=True))

    def add_tutor_to_client_request_pool(self, request_id, tutor_slug, price, update_status=False):
        params = {'tutor_slug': tutor_slug, 'cost': price}
        if update_status:
            params.update(update_status=True)
        return self._fetch_request(
            'POST', '{}/requestpool/add/'.format(request_id),
            json=params)

    def paid_processing_fee(self, pk):
        return self._fetch_request('POST', '{}/paid_processing_fee/'.format(pk))

    def admin_actions_on_profile_page(self, ts, request_pk, action):
        get = self._fetch_request
        base_url = '{}/'.format(request_pk)
        options = {
            1: lambda: get(
                'POST', '{}requestpool/add/'.format(base_url),
                json={'tutor_slug': ts['slug'], 'cost': ts['price']}),
            2: lambda: get(
                'PATCH', base_url,
                json={'tutor_slug': ts['slug'], 'status': 5}),
            3: lambda: get(
                'GET', '{}send_profile_to_client/'.format(base_url))
        }
        options[action]()

    def create_new_booking(self, request_id, **kwargs):
        return self._fetch_request(
            'POST',
            '{}/create_booking/'.format(request_id),
            json=kwargs)

    def get_booking_order(self, order):
        return self.deposit_service.get_booking_order(order)

    def update_wallet_and_notify_admin(self, order, new_amount):
        return self.deposit_service.update_wallet_and_notify_admin(
            order, new_amount)

    def get_authorization_url(self, order, **kwargs):
        _type = kwargs.pop('_type', None)
        if not _type:
            return self.deposit_service.get_authorization_url(order, **kwargs)
        return self._fetch_request(
            'POST', '{}/get_authorization_url/'.format(order),
            json=kwargs)

    def change_order(self, order):
        return self.deposit_service.change_order(order)

    def update_wallet_amount(self, order, **kwargs):
        return self.deposit_service.update_wallet_amount(order, **kwargs)

    def pricing_data(self, order):
        return self.deposit_service.pricing_data(order)

    def get_requests_with_filters(self, query="and", ** kwargs):
        data = {'query': query,
                'filters': kwargs}
        return self._fetch_request('POST', 'request_with_filters/', json=data)

    def update_requests_with_fields(self, req_ids, **kwargs):
        return self._fetch_request('POST', 'update_requests/', json={
            'req_ids': req_ids,
            'params': kwargs
        })

    def remove_all_requests(self):
        return self._fetch_request('POST', 'remove_all_data/', json={
            'remove_previous': True
        })


class DepositService(BaseRequestService):
    path = 'payments/'

    def __init__(self, *args, **kwargs):
        super(DepositService, self).__init__(*args, **kwargs)
        self.get_request_class()

    def get_booking_order(self, order):
        return self._fetch_request('GET',
                                   '{}/'.format(order))

    def update_wallet_and_notify_admin(self, order, new_amount):
        return self._fetch_request(
            'POST', '{}/update_wallet_and_notify_admin/'.format(order),
            json={'amount_paid': int(new_amount)})

    def get_authorization_url(self, order, **kwargs):
        return self._fetch_request(
            'POST', '{}/get_authorization_url/'.format(order),
            json=kwargs)

    def change_order(self, order):
        return self._fetch_request(
            'GET', '{}/reset_order/'.format(order))

    def update_wallet_amount(self, order, **kwargs):
        return self._fetch_request(
            'POST', '{}/update_wallet_amount/'.format(order),
            json=kwargs)

    def pricing_data(self, order):
        return self._fetch_request(
            'GET', '{}/get_different_prices/'.format(order))


class AdminHelpers(BaseGraphClient):
    string_params = """
    {
        slug
        email
        pk
        first_name
        last_name
        primary_phone_no
        location{
            vicinity
            state
            address
            latitude
            longitude
        }
        found_subjects {
            get_absolute_url
            name
        }
    }"""
    agent_params = """
    {
        id
        phone_number
        title
        image
        email
        name
    }
    """

    def get_skills_for_tutors(self, tutors):
        result = self._execute("""
            query getTutorSkills($slugs: [UserWithSubjectType]){
                users(slugs: $slugs)
                %s
            }
        """ % (self.string_params,), {
            "slugs": tutors}, "getTutorSkills")
        return self._get_response(result, "users")

    def get_skills_for_tutor(self, tutor):
        string_param = """
            query getTutor($slugs: UserWithSubjectType){
                user(slugs: $slugs)
                %s
            }""" % (self.string_params,)
        result = self._execute(
            string_param,
            {"slugs": tutor, }, "getTutor")
        return self._get_response(result, "user")

    def get_skills(self, skills):
        result = self._execute("""
        query getSkills($skills: SkillsInput){
            skills(skills: $skills){
                name
            }
        }
        """, {"skills": skills}, "getSkills")
        return self._get_response(result, "skills")

    def get_agent(self, agent_id=None):
        params = """
        {
            agent%s
            %s
        }
        """
        query = "query getAgent"
        if agent_id:
            data = params % ("(agent_id:$agent_id)", self.agent_params,)
            query += "($agent_id: Int)" + data
        else:
            data = params % ("", self.agent_params)
            query += data
        result = self._execute(query, {"agent_id": agent_id}, "getAgent")
        return self._get_response(result, "agent")

    def get_all_agents(self):
        result = self._execute("""
        query getAgents{
            agents
            %s
        }""" % (self.agent_params,), None, "getAgents")
        return self._get_response(result, "agents")
