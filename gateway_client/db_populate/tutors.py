from . import BaseTransferClient, populate
import importlib


def new_populate(cls, fields, data, extras=[]):
    w = populate(cls, fields, data, extras)
    w.username = data.slug
    return w


class DependsOnUserClient(BaseTransferClient):
    user_id = 'tutor_id'
    exclude_fields = ['owner', 'tutor', 'user', 'milestone']
    user_ids = []

    def condition(self, d):
        return getattr(d, self.user_id) in self.user_ids

    def get_sql_query(self):
        return (
                'SELECT "{0}".* FROM "{0}" '
                'INNER JOIN "auth_user" ON ("{0}"."{1}" = '
                '"auth_user"."id") INNER JOIN "users_userprofile" ON ('
                '"auth_user"."id" = "users_userprofile"."user_id") WHERE '
                '("users_userprofile"."application_status" > 0 AND NOT '
                '("{0}"."{1}" IS NULL))'.format(self.db_name, self.user_id))

    def get_foreign_keys(self):
        return [self.user_id]

    def populate_data(self):
        from django.db import IntegrityError
        fields = self.get_fields()

        data = [populate(self.model, fields, d) for d in self.get_query()
                if self.condition(d)]
        self.model.objects.bulk_create(data)
        print("{} Dump Completed".format(self.model.__class__.__name__))
        query = [d for d in self.get_query() if self.condition(d)]
        for rec in query:
            param = {key:getattr(rec, key) for key in self.get_foreign_keys()}
            try:
                self.model.objects.filter(
                    pk=rec.id).update(**param)
            except IntegrityError as e:
                pass
        print("{} Dump Completed".format(self.model.__class__.__name__))
        

class DependsOnTutorSkill(DependsOnUserClient):
    user_id = 'ts_id'
    exclude_fields = ['ts','tutor_skill']
    user_ids = []


def populate_all_subjects_data(settings, import_path="tutor_service.users.models"):
    models = importlib.import_module(import_path)
    TutorSkill = models.TutorSkill
    QuizSitting = models.QuizSitting
    SkillCertificate = models.SkillCertificate
    SubjectExhibition = models.SubjectExhibition

    class TutorSkillTransferClient(BaseTransferClient):
        model = TutorSkill
        exclude_fields = ['tutor','sitting','skillcertificate',
            'skillcertificate_set','exhibitions']
        db_name = "skills_tutorskill"
        include_fields = ['tutor_id']

    class SubjectExhibitionTransferClient(DependsOnTutorSkill):
        model = SubjectExhibition
        db_name = "skills_subjectexhibition"
        include_fields = ['ts_id']
        user_ids = TutorSkill.objects.values_list('pk', flat=True)


    class SkillCertificateTransferClient(DependsOnTutorSkill):
        model = SkillCertificate
        db_name = "skills_skillcertificate"
        include_fields = ['ts_id']
        user_ids = TutorSkill.objects.values_list('pk', flat=True)

    class QuizSittingTransferClient(DependsOnTutorSkill):
        model = QuizSitting
        user_id = 'tutor_skill_id'
        db_name = "skills_quizsitting"
        include_fields = ['tutor_skill_id']
        user_ids = TutorSkill.objects.values_list('pk',flat=True)

    tt = TutorSkillTransferClient(settings.HOST_URL)
    tt.populate_data()
    se = SubjectExhibitionTransferClient(settings.HOST_URL)
    se.populate_data()
    st = SkillCertificateTransferClient(settings.HOST_URL)
    st.populate_data()
    qt = QuizSittingTransferClient(settings.HOST_URL)
    qt.populate_data()


def populate_tutors_info(settings,no_to_populate=None, import_path="tutor_service.users.models"):
    models = importlib.import_module(import_path)

    class TutorBasicProfileTransferClient(BaseTransferClient):
        model = models.User
        db_name = "auth_user"
        exclude_fields = [
            'phonenumber', 'education', 'workexperience', 'guarantor', 'identifications', 'milestones', 'tutorskill',
            'image', 'image_approved', 'applications', 'auth_token', 'name', 'video_approved', 'image_approved',
            'socialmedia_connect',
        ]

        def get_sql_query(self):
            return ('SELECT *, "users_userprofile".custom_header as title FROM "auth_user" '
                    'INNER JOIN "users_userprofile" ON ("auth_user".id = "users_userprofile".user_id) '
                    'WHERE "users_userprofile".application_status > 0') 

        def populate_data(self):
            fields = self.get_fields()
            data = [new_populate(self.model, fields, d)
                    for d in self.get_query()]
            self.model.objects.bulk_create(data)
            print("{} Dump Completed".format(self.model.__class__.__name__))


    class PhoneNumberTransferClient(DependsOnUserClient):
        model = models.PhoneNumber
        db_name = "users_phonenumbers"
        user_id = 'owner_id'
        user_ids = models.User.objects.values_list('pk', flat=True)

    class EducationTransferClient(DependsOnUserClient):
        model = models.Education
        db_name = "tutors_educations"
        user_ids = models.User.objects.values_list('pk', flat=True)
        exclude_fields = DependsOnUserClient.exclude_fields +['country']


    class WorkExperienceTransferClient(DependsOnUserClient):
        model = models.WorkExperience
        db_name = "tutor_work_experiences"
        user_ids = models.User.objects.values_list('pk', flat=True)
        exclude_fields = DependsOnUserClient.exclude_fields + ['is_private']


    class GurantorTransferClient(DependsOnUserClient):
        model = models.Guarantor
        db_name = "tutor_guarantors"
        user_ids = models.User.objects.values_list('pk', flat=True)


    class UserIdenticationTransferClient(DependsOnUserClient):
        model = models.UserIdentification
        db_name = "users_identification"
        user_id = 'user_id'
        user_ids = models.User.objects.values_list('pk', flat=True)
        exclude_fields = DependsOnUserClient.exclude_fields + ['identity']

        
    class MilestoneTransferClient(BaseTransferClient):
        model = models.Milestone
        db_name = "rewards_milestone"
        exclude_fields = ['usermilestone']

    class UserMiletoneTransferClient(DependsOnUserClient):
        model = models.UserMilestone
        db_name = "users_usermilestone"
        user_id = 'user_id'
        user_ids = models.User.objects.values_list('pk', flat=True)
        
        def get_foreign_keys(self):
            return [self.user_id, 'milestone_id']


    uu = TutorBasicProfileTransferClient(settings.HOST_URL,no_to_populate=no_to_populate)
    uu.populate_data()

    pp = PhoneNumberTransferClient(settings.HOST_URL)
    pp.populate_data()

    aa = EducationTransferClient(settings.HOST_URL)
    aa.populate_data()
    
    bb = WorkExperienceTransferClient(settings.HOST_URL)
    bb.populate_data()

    cc = GurantorTransferClient(settings.HOST_URL)
    cc.populate_data()

    dd = UserIdenticationTransferClient(settings.HOST_URL)
    dd.populate_data()

    ee = MilestoneTransferClient(settings.HOST_URL)
    ee.populate_data()

    ff = UserMiletoneTransferClient(settings.HOST_URL)
    ff.populate_data()


def populate_all_data2(settings):
    pass