from __future__ import unicode_literals
from builtins import super
import os
import importlib
import records
import requests
from . import BaseTransferClient, populate


def get_requestpoolmodel(path):
    return importlib.import_module(path)


def populate_all_data(
        settings, import_path="tutor_service.connect_tutor.models"):
    models = get_requestpoolmodel(import_path)
    RequestPool = models.RequestPool
    Tutor = models.Tutor
    
    class RequestPoolTransferClient(BaseTransferClient):
        model = RequestPool
        exclude_fields = ["req", "tutor"]
        db_name = "connect_tutor_requestpool"
        user_id = 'tutor_id'

        def get_fields(self):
            return super().get_fields() + ["tutor_id"]

        def populate_data(self):
            fields = self.get_fields()
            data = [populate(self.model, fields, d) for d in self.get_query()
                    if self.condition(d)]
            self.model.objects.bulk_create(data)
            print("{} Dump Completed".format(self.model.__class__.__name__))
            
        def condition(self, d):
            return getattr(d, self.user_id) in Tutor.objects.values_list('pk', flat=True)

    uu = RequestPoolTransferClient(settings.HOST_URL)
    uu.populate_data()
    uu.update_modified_fields()
