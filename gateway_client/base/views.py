class ViewSetMixin(object):

    def _prepare_form_instance_with_data(self, request, instance):
        Form = self.get_serializer_class().get_form()
        return Form(request.data, instance=instance)

    def _validate_form_func(self, request, instance=None, save=False):
        from rest_framework import status
        from rest_framework.response import Response

        form = self._prepare_form_instance_with_data(request, instance)
        # import pdb; pdb.set_trace()
        form.is_valid()
        if save and form.is_valid():
            result = self._handle_after_save(request, form)
            return Response(
                self.serializer_class(
                    result, context={'request': request}).data,
                status=status.HTTP_201_CREATED)
        params = {'data': {'errors': form.errors,
                           'is_valid': form.is_valid()}}
        if save:
            params.update(status=status.HTTP_400_BAD_REQUEST)
        return Response(**params)

    def _handle_after_save(self, request, form):
        """What should happen to the form/formset after
        it is validated"""
        data = request.data.copy()
        post_action = data.pop('post_action', {})
        return form.after_save(post_action)
