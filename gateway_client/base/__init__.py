import warnings
# from nameko.standalone.rpc import ClusterRpcProxy
from .. import TuteriaApiException, BaseClient, get_field_value


class ApiClient(BaseClient):

    def __init__(self, username, url=None, broker_url=None):
        if url:
            self.base_url = url
        if broker_url:
            self.config = {'AMQP_URI': broker_url}
        if not self.config['AMQP_URI']:
            warnings.warn(("You didnt add a BROKER_URL. You should set this"
                           " as an environmental variable."))
        if not self.base_url:
            raise TuteriaApiException("Did you forget to pass the server url or"
                                      " set it as an environmental variable API_GATEWAY_SERVER_URL")
        self.username = username

    def get_transactions(self, name, key=None, filter_by=None):
        options = {
            'name': name,
            'fields': ['total', 'type', 'display', 'to_string', "amount"]
        }
        if key:
            options.update({'key': key, 'value': "%s" % filter_by, })
        return options

    def get_transaction_details(self, filter_by):
        return self.get_transactions('object_list', 'filter_by', filter_by)

    def get_revenue_details(self, filter_by="all_tutor"):
        data = {
            'name': 'wallet',
            'key': "username",
            'value': "%s" % self.username,
            "fields": ["total_earned", self.get_transactions("transactions"), "count", "total_withdrawn", "total_used_to_hire", "total_credit_used_to_hire", "amount_in_session",
                       "total_paid_to_tutor", "upcoming_earnings", self.get_transaction_details(filter_by)]
        }
        query = get_field_value(data)
        response = self._query_graphql_server(query)
        temp = response['wallet']
        temp.update(total_used_to_hire=temp['total_credit_used_to_hire'],
                    my_transactions=temp['count'] > 0,
                    wallet_balance=self.get_wallet_balance(temp),
                    withdrawable_balance=temp['amount_in_session'])
        for x in ["total_credit_used_to_hire", "total_paid_to_tutor", "amount_in_session", "transactions", "count"]:
            temp.pop(x, None)
        return temp

    def get_wallet_balance(self, temp):
        return temp['total_used_to_hire'] - (temp['amount_in_session'] + temp['total_paid_to_tutor'])

    def get_order_details(self, filter_by="all_client"):
        data = {
            'name': 'wallet',
            'key': "username",
            'value': self.username,
            "fields": ["amount_in_session", self.get_transactions("transactions"), "count", "total_paid_to_tutor", "total_used_to_hire",
                       "total_credit_used_to_hire",
                       self.get_transaction_details(filter_by)]
        }
        query = get_field_value(data)
        response = self._query_graphql_server(query)
        temp = response['wallet']
        temp.update(
            total_in_session=temp['amount_in_session'],
            total_credit_used=temp['total_credit_used_to_hire'],
            current_balance=self.get_wallet_balance(temp),
        )
        for x in ['amount_in_session', "total_used_to_hire", "total_credit_used_to_hire",
                  "transactions"]:
            temp.pop(x, None)
        return temp

    def trigger_payment(self, payment_type, ch=False, auto_withdraw=False):
        with ClusterRpcProxy(self.config) as rpc:
            rpc.payment_service.trigger_payment.async(
                username=self.username, payment_type=payment_type, ch=ch, auto_withdraw=auto_withdraw)


