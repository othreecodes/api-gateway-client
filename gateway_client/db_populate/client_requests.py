from __future__ import unicode_literals
from builtins import super

import os
import importlib
import records
import requests
from . import BaseTransferClient, populate


class PopulateSkill(object):
    @classmethod
    def populate(cls, import_path="request_service.client_requests.models"):
        models = importlib.import_module(import_path)
        Skill = models.Skill
        Skill.objects.bulk_create([
            Skill(_class=Skill.NURSERY, name='Elementary Mathematics',
                  verbose_name='Elementary Math'),
            Skill(_class=Skill.NURSERY, name='Elementary English'),
            Skill(_class=Skill.NURSERY, name='Literacy & Numeracy'),
            Skill(_class=Skill.NURSERY, name='Phonics',
                  verbose_name='Phonics & Orals', ),
            Skill(_class=Skill.NURSERY, name='General Nursery',
                  verbose_name='All Nursery Subjects'),
            Skill(_class=Skill.PRIMARY, name='Basic Mathematics'),
            Skill(_class=Skill.PRIMARY, name='English Language'),
            Skill(_class=Skill.PRIMARY, name='Basic Sciences'),
            Skill(_class=Skill.PRIMARY, name='Verbal Reasoning'),
            Skill(_class=Skill.PRIMARY, name='Quantitative Reasoning',
                  verbose_name='Quant. Reasoning'),
            Skill(_class=Skill.PRIMARY, name='Computer Education'),
            Skill(_class=Skill.JSS, name='General Mathematics',
                  verbose_name='Mathematics'),
            Skill(_class=Skill.JSS, name='English Language'),
            Skill(_class=Skill.JSS, name='Basic Sciences'),
            Skill(_class=Skill.JSS, name='Business Studies'),
            Skill(_class=Skill.JSS, name='Computer Science'),
            Skill(_class=Skill.JSS, name='Basic Technology'),
            Skill(_class=Skill.JSS, name='Agricultural Science'),
            Skill(_class=Skill.SSS, name='General Mathematics',
                  verbose_name='Mathematics'),
            Skill(_class=Skill.SSS, name='English Language'),
            Skill(_class=Skill.SSS, name='Physics'),
            Skill(_class=Skill.SSS, name='Further Mathematics'),
            Skill(_class=Skill.SSS, name='Chemistry'),
            Skill(_class=Skill.SSS, name='Literature in English'),
            Skill(_class=Skill.SSS, name='Economics'),
            Skill(_class=Skill.SSS, name='Commerce'),
            Skill(_class=Skill.SSS, name='Accounting'),
            Skill(_class=Skill.SSS, name='Government'),
            Skill(_class=Skill.SSS, name='Computer Science'),
            Skill(_class=Skill.SSS, name='Technical Drawing'),
            Skill(_class=Skill.SSS, name='Biology'),
            Skill(_class=Skill.SSS, name='Agricultural Science'),
            Skill(_class=Skill.SSS, name='Geography'),
            Skill(name='French Language', verbose_name='French'),
            Skill(name='Spanish Language', verbose_name='Spanish'),
            Skill(name='Piano'),
            Skill(name='Drums'),
            Skill(name='Guitar'),
            Skill(name='Saxophone'),
            Skill(name='Yoruba Language', verbose_name='Yoruba'),
            Skill(name='Hausa Language', verbose_name='Hausa'),
            Skill(name='Igbo Language', verbose_name='Igbo'),
        ])


def populate_all_data(
        settings, import_path="config.request_models",
        skill_path="request_service.client_requests.models"):
    models = importlib.import_module(import_path)
    BaseRequestTutor = models.BaseRequestTutor
    Skill = models.Skill
    Pricing = models.Pricing
    Region = models.Region
    DepositMoney = models.DepositMoney
    # from request_service.connect_tutor.models import RequestPool

    class DepositTransferClient(BaseTransferClient):
        model = DepositMoney
        exclude_fields = ['user', 'user_id', 'request']
        db_name = "external_depositmoney"

        def get_query(self):
            instance = super().get_query()
            current_requests = BaseRequestTutor.objects.values_list(
                'id', flat=True)
            return [x for x in instance if x.request_id in current_requests]

        def get_fields(self):
            return super().get_fields() + ['request_id']

        def update_modified_fields(self):
            for a in self.get_query():
                self.model.objects.filter(
                    order=a.order).update(modified=a.modified)
            print("{} modified".format(self.model.__class__.__name__))

    class RequestTransferClient(BaseTransferClient):
        model = BaseRequestTutor
        exclude_fields = ['user', 'request', 'payment_deposits', 'agent']

        def get_sql_query(self):
            return ("SELECT *, T3.slug as tutor_slug from external_baserequesttutor "
                    'LEFT OUTER JOIN "auth_user" T3 ON ("external_baserequesttutor"."tutor_id" = T3."id")')

        def update_requests_with_user(self):
            request_with_user = self.db.query(
                'SELECT user_id, id from "external_baserequesttutor"').all()
            ids_only = [r for r in request_with_user if r.id]
            for k in BaseRequestTutor.objects.filter(pk__in=[r.id for r in ids_only]).all():
                record = [x for x in ids_only if x.id == k.pk][0]
                BaseRequestTutor.objects.filter(
                    pk=k.pk).update(user=record.user_id)
            print("Completed")

    # class RequestPoolTransferClient(BaseTransferClient):
    #     model = RequestPool
    #     exclude_fields = ["req"]

    #     def get_sql_query(self):
    #         return ("SELECT *, T3.slug as tutor_slug from connect_tutor_requestpool "
    #                 'LEFT OUTER JOIN "auth_user" T3 ON ("connect_tutor_requestpool"."tutor_id" = T3."id")')

    #     def get_fields(self):
    #         return super().get_fields() + ['req_id']

    #     def get_tutor(self, slug):
    #         sql = ('SELECT "auth_user"."id", "auth_user"."email", "auth_user"."first_name", "auth_user"."last_name" '
    #                'FROM "auth_user" '
    #                'WHERE "auth_user"."slug" = \'{}\'').format(slug)
    #         return self.db.query(sql)

    #     def get_phone_number(self, tutor_id):
    #         sql = ('SELECT "users_phonenumbers"."number" '
    #                'FROM "users_phonenumbers" '
    #                'WHERE ("users_phonenumbers"."owner_id" = {} '
    #                'AND "users_phonenumbers"."primary" = True)').format(tutor_id)
    #         return self.db.query(sql)

    #     def get_valid_subjects(self, tutor_id):
    #         sql = ('SELECT "skills_skill"."related_with", "skills_skill"."name", "skills_skill"."slug" '
    #                'FROM "skills_tutorskill" INNER JOIN "skills_skill" '
    #                'ON ("skills_tutorskill"."skill_id" = "skills_skill"."id") '
    #                'WHERE ("skills_tutorskill"."tutor_id" = {} '
    #                'AND NOT ("skills_tutorskill"."status" = 4))').format(tutor_id)
    #         return self.db.query(sql).all()

    #     def get_locations(self, tutor_id):
    #         sql = ('SELECT "users_location"."state", "users_location"."vicinity", '
    #                '"users_location"."addr_type", "users_location"."address", '
    #                '"users_location"."latitude", "users_location"."longitude" FROM "users_location"'
    #                ' WHERE "users_location"."user_id" = {}').format(tutor_id)
    #         return self.db.query(sql).all()

    class PricingTransferClient(BaseTransferClient):
        model = Pricing
        db_name = "pricings_pricing"
        exclude_fields = ['region']

    class RegionTransferClient(BaseTransferClient):
        model = Region
        db_name = "pricings_region"
        exclude_fields = ['prices']

        def get_parent_skills(self):
            sql = ('SELECT "skills_skill"."name" FROM "skills_skill" '
                   'INNER JOIN "skills_skill_subcategories" '
                   'ON("skills_skill"."id"="skills_skill_subcategories"."skill_id") '
                   'INNER JOIN "skills_subcategory" '
                   'ON("skills_skill_subcategories"."subcategory_id"="skills_subcategory"."id") '
                   'WHERE "skills_subcategory"."section"=\'Academic Subjects\'')
            return [x.name for x in self.db.query(sql)]

    class RegionPricingTransferClient(BaseTransferClient):
        db_name = "pricings_region_prices"
        model = Region.prices.through

        def get_fields(self):
            return ["id", 'region_id', 'pricing_id']

    oo = RequestTransferClient(settings.HOST_URL)
    oo.populate_data()
    oo.update_modified_fields()

    vv = DepositTransferClient(settings.HOST_URL)
    vv.populate_data()
    vv.update_modified_fields()

    # uu = RequestPoolTransferClient(settings.HOST_URL)
    # uu.populate_data()
    # uu.update_modified_fields()

    pp = PricingTransferClient(settings.HOST_URL)
    pp.populate_data()

    rr = RegionTransferClient(settings.HOST_URL)
    rr.populate_data()

    xx = RegionPricingTransferClient(settings.HOST_URL)
    xx.populate_data()

    PopulateSkill().populate(skill_path)
