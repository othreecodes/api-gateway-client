import records


def populate(cls, fields, data, extras=[]):
    w = cls()
    for o in fields:
        data2 = getattr(data, o)
        setattr(w, o, data2)
    for x in extras:
        for key, value in x.items():
            data2 = getattr(data, value)
            setattr(w, key, data2)
    return w


class BaseTransferClient(object):
    model = None
    exclude_fields = []
    db_name = ""
    include_fields = []

    def __init__(self, db_url,no_to_populate=None):
        self.db = records.Database(db_url)
        self.no_to_populate = no_to_populate

    def get_sql_query(self):
        return "SELECT * from {}".format(self.db_name)

    def get_query(self):
        sql = self.get_sql_query()
        result = self.db.query(sql)
        if self.no_to_populate:
            result = result[:self.no_to_populate]
        return result

    def meta_fields(self):
        return self.model._meta.get_fields()

    def populate_data(self):
        fields = self.get_fields()
        data = [populate(self.model, fields, d) for d in self.get_query()]
        self.model.objects.bulk_create(data)
        print("{} Dump Completed".format(self.model.__class__.__name__))

    def update_modified_fields(self):
        for a in self.get_query():
            self.model.objects.filter(
                id=a.id).update(modified=a.modified)
        print("{} modified".format(self.model.__class__.__name__))

    def get_fields(self):
        if len(self.exclude_fields) > 0:
            return [f.name for f in self.meta_fields()
                    if f.name not in self.exclude_fields]
        return [f.name for f in self.meta_fields()] + self.include_fields
