from peewee import (Model, CharField, IntegerField, BooleanField, TextField,
                    ForeignKeyField, DateTimeField, JOIN)
import importlib
from playhouse.postgres_ext import PostgresqlDatabase
from playhouse.db_url import connect
import os
import logging
logger = logging.getLogger('peewee')
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler())

IntegrityError = importlib.import_module("django.db.utils.IntegrityError")
# MONOLITH DATABASE
BASE_DATABASE = connect(
    os.environ.get('BASE_DATABASE_URL')
    or 'postgres://tuteria:punnisher@127.0.0.1/tuteria')

# SERVICE DATABASE
SERVICE_DATABASE = connect(
    os.environ.get('QUIZ_SERVICE_DATABASE_URL')
    or 'postgres://tuteria:punnisher@127.0.0.1/test_tuteria_quiz_test')

# print(BASE_DATABASE)
# print(SERVICE_DATABASE)


class BaseModel(Model):
    class Meta:
        database = BASE_DATABASE


class Category(BaseModel):
    pk = IntegerField(db_column='id', primary_key=True)
    caption = CharField(null=True)
    featured = BooleanField()
    name = CharField()
    slug = CharField(index=True)

    class Meta:
        db_table = 'skills_category'
        schema = 'public'


class Skill(BaseModel):
    pk = IntegerField(db_column='id', primary_key=True)
    name = CharField()
    caption = CharField()
    description = TextField()
    duration = IntegerField()
    featured = BooleanField()
    heading = CharField()
    subheading = CharField()
    short_name = CharField()
    quiz = IntegerField(db_column='quiz_id')

    class Meta:
        db_table = 'skills_skill'
        schema = 'public'


class SkillCategories(BaseModel):
    category = ForeignKeyField(
        db_column='category_id', rel_model=Category, to_field='id')
    skill = ForeignKeyField(
        db_column='skill_id', rel_model=Skill, to_field='id')

    class Meta:
        db_table = 'skills_skill_categories'
        indexes = ((('skill', 'category'), True), )
        schema = 'public'


class SkillsTutorskill(BaseModel):

    skill = IntegerField(db_column='skill_id', verbose_name='skill')
    tutor = IntegerField(db_column='tutor_id')

    class Meta:
        db_table = 'skills_tutorskill'
        indexes = ((('tutor', 'skill'), True), )
        schema = 'public'


class SkillsQuizsitting(BaseModel):
    completed = BooleanField()
    created = DateTimeField()
    modified = DateTimeField()
    score = IntegerField()
    started = BooleanField()
    tutor_skill = ForeignKeyField(
        db_column='tutor_skill_id', rel_model=SkillsTutorskill, to_field='id')

    class Meta:
        db_table = 'skills_quizsitting'
        schema = 'public'


class QuizQuiz(BaseModel):
    description = TextField()
    pass_mark = IntegerField()
    title = CharField()
    url = CharField(index=True)

    class Meta:
        db_table = 'quiz_quiz'
        schema = 'public'

class Question(BaseModel):
    pass

class Answer(BaseModel):
    pass

def populate_all_quiz_data(settings,
                           import_path="skill_quiz_service.models.quiz"):
    models = importlib.import_module(import_path)
    _quiz = models.Quiz

    for x in QuizQuiz.select().dicts():
        try:
            s = _quiz.objects.create(**x)
            s.save()
        except IntegrityError as e:
            print(e)


def populate_all_categories_data(settings,
                                 import_path="skill_quiz_service.models"):
    models = importlib.import_module(import_path)
    _category = models.Category
    # data = [x for x in Category.select().dicts()]

    for x in Category.select().dicts():
        try:
            s = _category.objects.create(**x)
            s.save()
        except IntegrityError as e:
            print(e)


def populate_all_skill_data(settings, import_path="skill_quiz_service.models"):
    models = importlib.import_module(import_path)
    _skill = models.Skill

    for x in Skill.select().dicts():
        try:
            s = _skill.objects.create(**x)
            s.save()
        except IntegrityError as e:
            print(e)


def populate_all_skill_categories_data(
        settings, import_path="skill_quiz_service.models"):
    models = importlib.import_module(import_path)
    _skill = models.Skill
    d = SkillCategories.select().dicts()
    # x = [o for o in d]

    for obj in d:
        try:
            s = _skill.objects.get(pk=obj['skill'])
            s.category_id = obj['category']
            s.save()
        except IntegrityError as e:
            print(e)


def populate_all_sitting_data(settings,
                              import_path="skill_quiz_service.models"):
    models = importlib.import_module(import_path)
    _quizsitting = models.QuizSitting
    xs = SkillsQuizsitting.raw(
        'SELECT t1.id, t1.completed, t1.created, t1.modified, t1.score, t1.started, b.tutor_id, b.skill_id FROM skills_quizsitting AS t1 FULL OUTER JOIN skills_tutorskill AS b on t1.tutor_skill_id = b.id '
    ).dicts()

    for row in xs:

        try:
            qs = _quizsitting.objects.create(
                id=row['id'],
                completed=row['completed'],
                created=row['created'],
                modified=row['modified'],
                score=row['score'],
                started=row['started'],
                tutor=row['tutor_id'],
                skill_id=row['skill_id'])
            qs.save()
        except IntegrityError as e:
            print(e)


if __name__ == '__main__':

    # db = BASE_DATABASE
    # # cursor = db.execute_sql('SELECT t1.id, t1.completed, t1.created, t1.modified, t1.score, t1.started, t2.id FROM public.skills_quizsitting AS t1 INNER JOIN public.skills_tutorskill AS t2;', [])

    # cursor = db.execute_sql(
    #     'SELECT t1.id, t1.completed, t1.created, t1.modified, t1.score, t1.started, b.tutor_id, b.skill_id FROM skills_quizsitting AS t1 FULL OUTER JOIN skills_tutorskill AS b on t1.tutor_skill_id = b.id ')
    # # cursor = db.execute_sql('')
    # rows = cursor.fetchone()
    # do = SkillsQuizsitting.select().join(SkillsTutorskill,join_type=JOIN.FULL)

    # xs = SkillsQuizsitting.raw(
    #     'SELECT t1.id, t1.completed, t1.created, t1.modified, t1.score, t1.started, b.tutor_id, b.skill_id FROM skills_quizsitting AS t1 FULL OUTER JOIN skills_tutorskill AS b on t1.tutor_skill_id = b.id ; SET tutor = 1 '
    # ).dicts()

    # x = [o for o in xs]

    xs = SkillsQuizsitting.raw(
        'SELECT t1.id, t1.completed, t1.created, t1.modified, t1.score, t1.started, b.tutor_id, b.skill_id FROM skills_quizsitting AS t1 FULL OUTER JOIN skills_tutorskill AS b on t1.tutor_skill_id = b.id '
    ).dicts()

    n = 0
    for x in xs:
        if x['created'] is None:
            pass
            # print(1)
        else:
            n = n + 1
            print(x['created'])
            print(x)

    print(n)
