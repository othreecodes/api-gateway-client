import warnings
from . import (BaseClient, TuteriaApiException, get_field_value,
               BaseGraphClient)


class LocationGrpah(BaseGraphClient):
    path = 'graphql'

    def location_query(self):
        query = """
        {
            user_locations(user_id:$user_id){
                country
                location{
                    id
                    address
                    area
                    state
                    vicinity
                    longitude
                    latitude
                }
            }
        }
        """
        if not self.change:
            query = """
            query getUserLocations($user_id: Int!)
            """+ query
        return query

    def _execute(self, *args):
        if self.change:
            query, first_args = self._transform_query(*args)
            return self.client.execute(query, first_args)
        return self.client.execute(*args)

    def get_locations(self, user_id):
        result = self._execute(
            self.location_query(),
            {"user_id": user_id}, "getUserLocations")
        return self._get_response(result, "user_locations")


class LocationAPIClient(BaseClient):
    def __init__(self, url=None, broker_url=None):
        if url:
            self.base_url = url
        if broker_url:
            self.config = {'AMQP_URI': broker_url}
        if not self.config['AMQP_URI']:
            warnings.warn(("You didnt add a BROKER_URL. You should set this"
                           " as an environmental variable."))
        if not self.base_url:
            raise TuteriaApiException("Did you forget to pass the server url or"
                                      " set it as an environmental variable API_GATEWAY_SERVER_URL")
        self.service = LocationGrpah(self.base_url)
        self.get_request_class()

    def get_locations(self, user_id):
        result = self.service.get_locations(user_id)
        return result

    def resolve_path(self, **kwargs):
        path = "locations/"
        sid = kwargs.pop('location_id', '')
        if sid:
            path = '{}{}/'.format(path, sid)
        return path, kwargs
