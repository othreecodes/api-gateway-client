import os
import warnings
import json
from nameko.standalone.rpc import ClusterRpcProxy

from gateway_client import BaseGraphClient
from gateway_client import tutor, location
from .base import BaseClient, TuteriaApiException, get_field_value


class BookingGraph(BaseGraphClient):
    path = "graphql"

    def get_bookings_query(self, query_variables, with_sessions):
        query = """
            {
            bookings%s{
            
            """

        query = query + """
            order
            skill_display
            status
            booking_type
            first_session
            last_session
            bank_price
            student_no

        """
        if with_sessions:
            query = query + """
            bookingsession_set{
                start 
                no_of_hours 
                status 
                price 
                student_no                    
                }
            """
        
        query = query + """        
            }
            }
        """ 
        
        query = query % ("(" + ",".join(
            '%s:$%s' % t
            for t in zip(query_variables, query_variables)) + ")"
                if query_variables else "")

        if not self.change:
            query = """
                    query getAllUserBookings
                    """ + query

        return query

    def get_single_booking_query(self, with_sessions):

        query = """
            {
            booking(order:$order) {
            
            """

        query = query + """
            order
            skill_display
            status
            booking_type
            first_session
            last_session
            total_price
            student_no
            """

        if with_sessions:
            query = query + """
            bookingsession_set{
                    start 
                    no_of_hours 
                    status 
                    price 
                    student_no
                    
                }
            """

        query = query + """
                }
            }
        """

        if not self.change:
            query = """
                    query getBooking($order: String!)
                    """ + query
        return query

    def get_booking_sessions_query(self):
        query = """
            {
            booking(order:$order) {
                bookingsession_set{
                start 
                no_of_hours 
                status 
                price 
                student_no
                
                }
                }
            }

        """
        if not self.change:
            query = """
                    query getBookingSessions($order: String!)
                    """ + query
        return query

    def get_bookingsessions(self, pk):
        result = self._execute(self.get_booking_sessions_query(),
                               {"order": pk}, "getBookingSessions")
        # import pdb;pdb.set_trace()
        result = self._get_response(result, "booking")
        if result:
            return result.get('bookingsession_set', None)
        return None

    def get_booking_by_order(self, pk, with_sessions):
        result = self._execute(
            self.get_single_booking_query(with_sessions), {"order": pk},
            "getBooking")
        return self._get_response(result, "booking")

    def get_all_user_bookings(self, **kwargs):
        filters = {}
        query_variables = []
        with_sessions = kwargs.get('with_sessions', False)
        if kwargs.get('tutor'):
            query_variables.append('tutor')
            filters['tutor'] = kwargs.get('tutor')

        if kwargs.get('client'):
            query_variables.append('client')
            filters['client'] = kwargs.get('client')

        result = self._execute(
            self.get_bookings_query(query_variables, with_sessions), filters,
            "getUserBookings")
        return self._get_response(result, "bookings")

    def create_booking_query(self, query_variables):
        mutation = '''
        mutation {
                  create_booking%s {
                    message
                    errors
                    data {

                      status
                      order

                      user {
                        email
                      }
                      tutor {
                        email                
                     }
                    }
                  }
                }
        ''' % ("(" + ",".join('%s:$%s' % t
                              for t in zip(query_variables, query_variables)) +
               ")" if query_variables else "")
        if not self.change:
            mutation = """
                    mutation createBooking
                    """ + mutation
        return mutation

    def update_booking_query(self, **kwargs):
        params = "(order:$order, sessions: $sessions, review: $review, status: $status)"

        actual_query = """{
            update_booking%s {
                message
                errors
                data {
                    order
                    status
                    first_session
                    last_session
                    reviewed
                    description
                    made_payment
                    delivered_on
                    student_no
                    reviews_given {
                        review
                        score
                        review_type
                    }
                    bookingsession_set {
                    status
                    }

                }
            }
          }""" % params
        self.query = "mutation updateBooking"
        if self.change:
            self.query = self.query + \
                "($order: String!, $sessions:[SessionInput], $review:GenericScalar, $status:String)"
        self.query = self.query + actual_query

        if self.change:
            result = self.client.execute(
                self.query,
                variable_values=kwargs,
                operation_name="updateBooking")
        else:
            result = self._execute(self.query, kwargs, "updateBooking")
        
        return self._get_response(result, "update_booking")

    def close_booking_query(self, **kwargs):
        params = "(order:$order)"

        actual_query = """{
            close_booking%s {
                message
                errors
                data {
                    order
                    status
                    first_session
                    last_session
                    reviewed
                    description
                    made_payment
                    delivered_on
                    student_no
                    reviews_given {
                        review
                        score
                        review_type
                    }
                    bookingsession_set {
                    status
                    }

                }
            }
          }""" % params
        self.query = "mutation closeBooking"
        if self.change:
            self.query = self.query + \
                "($order: String!)"
        self.query = self.query + actual_query

        if self.change:
            result = self.client.execute(
                self.query,
                variable_values=kwargs,
                operation_name="closeBooking")
        else:
            result = self._execute(self.query, kwargs, "closeBooking")
        # import pdb ; pdb.set_trace()
        return self._get_response(result, "close_booking")

    def create_booking(self, **kwargs):
        params = "(tutor:$tutor,client:$client,percentage_split:$percentage_split,subjects:$subjects,sessions:$sessions)"
        actual_params = """{
            create_booking%s {
                message
                errors
                data {

                    status
                    order

                    user {
                    email
                    }
                    tutor {
                    email                
                    }
                }
            }
        }
        """ % params
        self.query = "mutation createBooking"

        self.query = self.query + \
            "($sessions: [SessionInput], $subjects: [String], $tutor: String, $client: String, $percentage_split: Int)"
        self.query = self.query + actual_params

        if self.change:
            result = self.client.execute(
                self.query,
                variable_values=kwargs,
                operation_name="createBooking")
        else:
            result = self._execute(self.query, kwargs, "createBooking")

        return self._get_response(result, "create_booking")

    def _execute(self, *args):
        if self.change:
            query, first_args = self._transform_query(*args)

            return self.client.execute(query, first_args)

        return self.client.execute(*args)


class BookingAPIClient(BaseClient):
    def __init__(self, order, url=None, broker_url=None):
        if url:
            self.base_url = url

        self.tutor_service_url = os.environ.get("TUTOR_SERVICE_URL", "/")
        self.location_service_url = os.environ.get("LOCATION_SERVICE_URL", "/")
        if broker_url:
            self.config = {'AMQP_URI': broker_url}
        if not self.config['AMQP_URI']:
            warnings.warn(("You didnt add a BROKER_URL. You should set this"
                           " as an environmental variable."))
        if not self.base_url:
            raise TuteriaApiException(
                "Did you forget to pass the server url or"
                " set it as an environmental variable API_GATEWAY_SERVER_URL")
        self.order = order

        self.service = BookingGraph(self.base_url)
        self.get_request_class()

    def get_weekday_elapsed(self):
        data = {
            'name': 'booking',
            'key': "order",
            'value': "%s" % self.order,
            "fields": [
                "weeks_elapsed",
            ]
        }
        query = get_field_value(data)
        response = self._query_graphql_server(query)
        temp = response['booking']
        return temp['weeks_elapsed']

    def get_tutor_level(self, email):
        data = {
            'name': 'tutor',
            'key': "email",
            'value': "%s" % email,
            "fields": ["admin_cut"]
        }
        query = get_field_value(data)
        response = self._query_graphql_server(query)
        return response['tutor']['admin_cut']

    def pay_tutor(self, amount_completed, refund, cancel_initiator=None):
        with ClusterRpcProxy(self.config) as rpc:
            rpc.payment_service.pay_tutor_with_booking.async(
                order=self.order,
                amount_completed=amount_completed,
                refund=refund,
                cancel_initiator=cancel_initiator)

    @classmethod
    def create_payment_wallet_transaction(cls, booking, broker_url=None):
        if broker_url:
            cls.config = {'AMQP_URI': broker_url}
        with ClusterRpcProxy(cls.config) as rpc:
            rpc.payment_service.create_payment_wallet_transaction.async(
                **booking)

    def after_booking_has_been_paid(self):
        with ClusterRpcProxy(self.config) as rpc:
            rpc.payment_service.after_booking_has_been_paid.async(
                order=self.order)

    def remove_none_values(self, d):
        return dict(
            filter(lambda x: x[1], d.items())) if d is not None else None

    def get_bookings(self, **kwargs):

        result = self.service.get_all_user_bookings(**kwargs)
        if result == []:
            raise TuteriaApiException("No bookings found")
        return result

    def get_single_booking(self, order, with_sessions=False):
        result = self.remove_none_values(
            self.service.get_booking_by_order(order, with_sessions))
        if result is None:
            raise TuteriaApiException("No such booking exists")
        return result

    def update_booking(self, **kwargs):
        result = self.remove_none_values(
            self.service.update_booking_query(**kwargs))
        is_valid = True
        if result.get('errors'):
            if 'The booking does not exist' in result.get('errors'):
                raise TuteriaApiException(str(result.get('errors')))
            else:
                is_valid = False
                return result, is_valid

        return result, is_valid

    def close_booking(self, **kwargs):
        is_valid = True
        result = self.remove_none_values(self.service.close_booking_query(**kwargs))

        if result.get('errors'):
            if 'The booking does not exist' in result.get('errors'):
                raise TuteriaApiException(str(result.get('errors')))
            else:
                is_valid = False
                return result, is_valid
        return result, is_valid            

    def get_sessions(self, order):
        return self.service.get_bookingsessions(order)

    def create_booking(self, **kwargs):

        return self.remove_none_values(self.service.create_booking(**kwargs))

    def get_sessions(self, order):
        result = self.service.get_bookingsessions(order)
        if result is None:
            raise TuteriaApiException(
                "No sessions exist for booking with order {0}".format(order))
        return result

    def create_booking(self, **kwargs):
        is_valid = True
        result = self.remove_none_values(self.service.create_booking(**kwargs))
        if result.get('errors'):
            is_valid = False

            return result.get('errors'), is_valid
        return result, is_valid

    def get_booking_urls(self):
        data = {
            'name': 'booking',
            'key': "order",
            'value': self.order,
            "fields": ["get_absolute_url", "get_tutor_absolute_url"]
        }
        query = get_field_value(data)
        response = self._query_graphql_server(query)
        return response['booking']

    def mail_client_and_tutor_on_successful_booking(self):
        booking = self.get_single_booking(self.order)

        tutorapi = tutor.TutorAPIClient(url=self.tutor_service_url)

        tutor_ = tutorapi.get_user(username=booking.tutor.username)

        location_ = location.LocationAPIClient(url=self.location_service_url)

        user_location = location_.get_locations(tutor_['id'])
        phone_num = tutorapi.get_user_phonenumbers(
            username=booking.tutor.username)

        booking_dict = {
            'skill_display': booking['skill_display'],
            'due_date': booking['due_date'],
            'get_tutor': {
                'email': booking['tutor']['email'],
                'vicinity': user_location[0]['location']['vicinity'],
                'first_name': booking['tutor']['first_name'],
                'last_name': booking['tutor']['last_name'],
                'location': user_location[0]['location']['vicinity'],
                'get_full_name': booking['tutor']['full_name'],
                'primary_phone_no': {
                    'number': str(phone_num[0]['number'])
                },
            },
            'discount_percent': booking['discount_percent'],
            'student_no': booking['student_no'],
            'get_absolute_url': "",  # TODO: get booking url
            'get_tutor_absolute_url': "",  # TODO: get tutor url
            'total_price': float(booking['total_price']),
            'first_session': booking['firstSession'],
            'get_price': float(booking['get_price']),
            'tutor_pricing': float(booking['tutor_pricing']),
            'sessions_count': len(booking['bookingsessionSet']),
            'bank_price': float(booking['bank_price']),
            'real_price': float(booking['real_price']),
            'total_hours': float(booking['total_hours']),
            'user': {
                'email': booking['user']['email'],
                'vicinity': "",  # TODO: Somehow get users vicinity
                'first_name': booking['user']['first_name'],
                'location': "",  # TODO: get users location
                'get_full_name': booking['user']['full_name']
            },
            'last_session': booking['lastSession'],
            'order': booking['order']
        }

        sms_options_tutor = {
            'sender':
            str(phone_num[0]['number']),  # TODO:Mistake?
            'body':
            ("{} in {} has hired you for {} lessons. "
             "Go to tuteria.com/dashboard immediately for full details!"
             ).format(booking.user.first_name, "booking.user.vicinity",
                      booking.skill_display()),
            'receiver':
            str(phone_num[0]['number']),
        }

    def send_mail_to_client_on_cancelled_booking(self):
        pass

    def send_money_to_tutor(self):
        pass

    def send_mail_to_client_on_successful_booking(self):
        pass

    def send_mail_to_client_on_cancelled_booking_session(self):
        pass
