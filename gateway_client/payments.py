import os
import json
import requests
from . import TuteriaApiException, BaseClient, get_field_value, FormField
from nameko.standalone.rpc import ClusterRpcProxy
from dateutil import parser


class PaymentService(BaseClient):

    def __init__(self, url=None, broker_url=None):
        self.base_url = os.environ.get('PAYMENT_SERVICE_URL')
        self.broker_url = os.environ.get('MESSAGING_URL')
        self.path = '/forms/userpayout/'
        if url:
            self.base_url = url
        if broker_url:
            self.broker_url = broker_url
        if not self.base_url:
            raise TuteriaApiException("Url for Payment Service was not found.")

    def get_form_parameters(self):
        response = requests.get(self.base_url + self.path)
        response.raise_for_status()
        result = response.json()
        return FormField(result)

    def validate(self, cleaned_data, save=False):
        data = cleaned_data
        data.update(save=save)
        response = requests.post(self.base_url + self.path + 'validate/',
                                 json=data)
        response.raise_for_status()
        return response.json()

    def get_payouts(self, username):
        data = {
            'name': 'userpayout',
            'key': "username",
            'value': "%s" % username,
            "fields": ["payout_type", "account_name",
                       "bank", "account_id"]
        }
        query = get_field_value(data)
        response = self._query_graphql_server(query)
        return response['userpayout']

    def get_revenues(self, username, filter_by="revenues"):
        """Returns the revenues transaction for a user filtered"""
        response = self.get_transaction_results(
            username, filter_by, 'revenues')
        values_list = response['wallet']['transactions']
        return values_list

    @classmethod
    def construct_query(cls, username, filter_by, request_type=None):
        """Constructs the graphql query for the transactions call"""
        options = {
            'orders': ["total_used_to_hire", "total_in_session", "total_credit_used", "total_paid_to_tutor"
                       ],
            'revenues': [
                'total_earned', 'total_withdrawn', 'total_used_to_hire', 'total_bonus_credit',
                'amount_available', 'count',
                "total_paid_to_tutor", 'upcoming_earnings',  "amount_in_session", "total_in_session"]
        }
        other_params = []
        try:
            other_params = options[request_type]
        except KeyError:
            other_params = []
        data = {
            'name': 'wallet',
            'key': 'username',
            'value': "%s" % username,
            'fields': other_params + [
                {
                    'name': 'transactions',
                    'key': "filter_by",
                    'value': "%s" % filter_by,
                    'fields': ["created", 'to_string', 'transaction_type',
                               'total', {'name': 'booking', 'fields': ['get_absolute_url']}]}]
        }
        return get_field_value(data)

    def get_orders(self, username, filter_by="orders"):
        """Returns the orders transaction for a user filtered"""
        response = self.get_transaction_results(username, filter_by, 'orders')
        values_list = response['wallet']['transactions']
        return values_list

    def get_transaction_results(self, username, filter_by, request_type):
        options = {
            'revenues': "wallet_balance",
            "orders": "current_balance"
        }
        field = options[request_type]
        query = self.construct_query(username, filter_by, request_type)
        result = self._query_graphql_server(query)
        wallet = result['wallet']
        # import pdb
        # pdb.set_trace()
        values_list = wallet['transactions']
        for _dict in values_list:
            if 'modified' in _dict.keys():
                _dict['modified'] = parser.parse(_dict['modified'])
            if 'created' in _dict.keys():
                _dict['created'] = parser.parse(_dict['created'])

        wallet.update(transactions=values_list)
        wallet.update({field: calculate_wallet_balance(wallet, field)})
        result.update(wallet=wallet)
        return result

    @property
    def config(self):
        return {'AMQP_URI': self.broker_url}

    @classmethod
    def total_amount_earned_by_tutors(cls, broker_url):
        """Returns the total amount tutors have earned"""
        config = {'AMQP_URI': broker_url}
        with ClusterRpcProxy(config) as rpc:
            result = rpc.payment_service.get_total_tutor_earnings()
        return result

    def rpc_helper(self, as_async, **kwargs):
        """Determines the caller and passes the other params"""
        import inspect
        func = inspect.currentframe().f_back.f_code.co_name
        with ClusterRpcProxy(self.config) as rpc:
            the_func = getattr(rpc.payment_service, func)
            if as_async:
                the_func.async(**kwargs)
            else:
                the_func(**kwargs)

    def update_amount_available(self, username, default):
        self.rpc_helper(True, username=username, default=default)

    def update_paystack_auth_code(self, username, code):
        self.rpc_helper(True, username=username, auth_code=code)

    def trigger_payment(self, username, payment_type, **kwargs):
        self.rpc_helper(False, username=username,
                        payment_type=payment_type, **kwargs)


def calculate_wallet_balance(wallet, field):
    if field == "revenues":
        return wallet['total_used_to_hire'] - \
            (wallet['amount_in_session'] + wallet['total_paid_to_tutor'])
    return wallet['total_used_to_hire'] - \
        (wallet['total_in_session'] + wallet['total_paid_to_tutor'])
